package com.example.smartcityone;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivityDenglu extends AppCompatActivity {
TextView tvDenglu,tvWangluo;
EditText etUsername,etPassword;

CheckBox checkBox;

OkHttpClient client=new OkHttpClient();
MediaType JSON=MediaType.get("application/json; charset=utf-8");
    double longitude = 101.6656;
    double latitude = 39.2072;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_denglu);
        tvDenglu=findViewById(R.id.tvDenglu);
        tvWangluo=findViewById(R.id.tvWangluo);
        etUsername=findViewById(R.id.etUsername);
        etPassword=findViewById(R.id.etPassword);
        checkBox=findViewById(R.id.checkBox);

        tvWangluo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivityDenglu.this, MainActivityWangluo.class);
                startActivity(intent);
            }
        });
        tvDenglu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=etUsername.getText().toString();
                String password=etPassword.getText().toString();
                login(username,password);
            }
        });

        set();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                    {
                        save(true);
                    }
                    else
                    {
                        save(false);
                    }
            }
        });
    }


    private String getWeather(double longitude, double latitude) {
        OkHttpClient okHttpClient = new OkHttpClient();
        String url = "https://api.caiyunapp.com/v2.6/2zTXWmFruEPIJs5l/" + longitude + "," + latitude + "/daily?dailysteps=1";
        Request request = new Request.Builder().url(url).build();
        Response response = null;
        try {
            // 执行网络请求
            response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                // 请求成功，打印并返回响应体内容
                String body = response.body().string();
                System.out.println(body);
                return body;
            } else {
                // 请求失败，打印错误信息
                System.out.println("Request failed with code: " + response.code() + " and message: " + response.message());
                return "Request failed with code: " + response.code() + " and message: " + response.message();
            }
        } catch (Exception e) {
            // 捕获并打印异常信息
            e.printStackTrace();
            return "Request failed due to exception: " + e.getMessage();
        } finally {
            if (response != null) {
                // 确保响应关闭
                response.close();
            }
        }
    }


    private void login(String username,String password)
    {
        JSONObject jsonObject=new JSONObject();
        try {
         jsonObject.put("Account",username);
         jsonObject.put("Password",password);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        RequestBody body=RequestBody.create(jsonObject.toString(),JSON);System.out.println(jsonObject.toString());
        Request request=new Request.Builder().url("https://api.nlecloud.com/Users/Login").post(body).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                System.out.println("失败1");
            }
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.isSuccessful())
                {
                    ResponseBody responseBody=response.body();
                    if (responseBody!=null)
                    {
                        String responseBodystring=responseBody.string();
                        System.out.println(responseBodystring);

                    }
                    else
                    {
                        System.out.println("body为空");
                    }
                }
                else
                {
                    System.out.println("失败2");
                }
            }
        });
    }

    private void save(boolean remember)
    {
        SharedPreferences sharedPreferences=getSharedPreferences("a", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        if (remember)
        {
            editor.putString("username",etUsername.getText().toString());
            editor.putString("password",etPassword.getText().toString());
            editor.putBoolean("isJizhu",true);
        }
        else
        {
            editor.remove("username");
            editor.remove("password");
            editor.putBoolean("isJizhu",false);
        }
        editor.apply();
    }

    private void set()
    {
        SharedPreferences sharedPreferences=getSharedPreferences("a",Context.MODE_PRIVATE);
        boolean isJizhu=sharedPreferences.getBoolean("isJizhu",false);
        checkBox.setChecked(isJizhu);
        if (isJizhu)
        {
            etUsername.setText(sharedPreferences.getString("username",""));
            etPassword.setText(sharedPreferences.getString("password",""));
        }
    }
}
