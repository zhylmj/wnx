package com.example.smartcityone;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
ViewPager viewPager;
PagerAdapter pagerAdapter;
View view,view2,view3;
List<View>list;
int index=0;
int flag=0;
Handler handler=new Handler();
TextView tvKaishi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager=findViewById(R.id.viewPager);
        LayoutInflater inflater=getLayoutInflater();
        view=inflater.inflate(R.layout.one,null);
        view2=inflater.inflate(R.layout.two,null);
        view3=inflater.inflate(R.layout.three,null);
        tvKaishi=view3.findViewById(R.id.tvKaishi);
        list=new ArrayList<>();
        list.add(view);
        list.add(view2);
        list.add(view3);

        tvKaishi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, MainActivityDenglu.class);
                startActivity(intent);
            }
        });
        handler.postDelayed(runnable,2000);

        pagerAdapter=new PagerAdapter() {
            @Override

            public int getCount() {
                return list.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view==object;
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
                container.removeView(list.get(position));
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup container, int position) {
                container.addView(list.get(position));
                return list.get(position);
            }
        };
        viewPager.setAdapter(pagerAdapter);
    }
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
                  flag++;
                  if (flag>=viewPager.getAdapter().getCount())
                  {
                      flag=0;
                  }
                  viewPager.setCurrentItem(flag);
                  handler.postDelayed(this,2000);
        }
    };

}
