package com.example.smartcityone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivityWangluo extends AppCompatActivity {
TextView tvBaochun;
EditText etAddress,etDuankou;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_wangluo);
        tvBaochun=findViewById(R.id.tvBaochun);
        etAddress=findViewById(R.id.etAdreess);
        etDuankou=findViewById(R.id.etDuankou);

        Set();


        tvBaochun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Save();

                Intent intent=new Intent(MainActivityWangluo.this, MainActivityDenglu.class);
                startActivity(intent);
            }
        });
    }
    private void Save()
    {
        SharedPreferences sharedPreferences=getSharedPreferences("aa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("address",etAddress.getText().toString());
        editor.putString("port",etDuankou.getText().toString());
        editor.apply();
    }
    private void Set()
    {
        SharedPreferences sharedPreferences=getSharedPreferences("aa",Context.MODE_PRIVATE);
        etAddress.setText(sharedPreferences.getString("address",""));
        etDuankou.setText(sharedPreferences.getString("port",""));
    }

}